#!/bin/bash
#firewall-script hast to run as root at startuü
#
#   line for roots crontab:
#   @reboot /home/locke/.config/autostart/firewall.sh
#

set -e

# Nichts tun, wenn loopback aktiviert wird
# [ "$IFACE" != "lo" ] || exit 0


#
# IPv4
#

# alte Konfiguration loeschen
/sbin/iptables -t mangle -F
/sbin/iptables -t mangle -X
/sbin/iptables -t nat -F
/sbin/iptables -t nat -X
/sbin/iptables -F
/sbin/iptables -X

# forwarding aktivieren
echo 0 > /proc/sys/net/ipv4/ip_forward

# loopback freischalten
/sbin/iptables -A INPUT -i lo -j ACCEPT
/sbin/iptables -A OUTPUT -o lo -j ACCEPT

# Antworten auf bestehende Verbindungen erlauben
/sbin/iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

# ICMP fuer Management der Netzwerkverbindung erlauben
# ICMP-Antwortpakete erlauben
/sbin/iptables -A INPUT -p icmp -m icmp --icmp-type echo-reply -j ACCEPT
/sbin/iptables -A INPUT -p icmp -m icmp --icmp-type echo-request -j ACCEPT
/sbin/iptables -A INPUT -p icmp -m icmp --icmp-type destination-unreachable -j ACCEPT

# Standards
/sbin/iptables -P INPUT  DROP
/sbin/iptables -P FORWARD DROP
/sbin/iptables -P OUTPUT ACCEPT

# Alle Pakete ordentlich zurückweisen
/sbin/iptables -A INPUT -p tcp -j REJECT --reject-with tcp-reset
/sbin/iptables -A INPUT -j REJECT --reject-with icmp-port-unreachable



#
# IPv6
#

# alte Konfiguration loeschen
/sbin/ip6tables -t mangle -F
/sbin/ip6tables -t mangle -X
/sbin/ip6tables -t nat -F
/sbin/ip6tables -t nat -X
/sbin/ip6tables -F
/sbin/ip6tables -X

# loopback freischalten
/sbin/ip6tables -A INPUT -i lo -j ACCEPT
/sbin/ip6tables -A OUTPUT -o lo -j ACCEPT
 
# Antworten auf bestehende Verbindungen erlauben
#/sbin/ip6tables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

# ICMP-Antwortpakete erlauben
/sbin/ip6tables -A INPUT -p icmp -j ACCEPT

# Standards
/sbin/ip6tables -P INPUT DROP
/sbin/ip6tables -P FORWARD DROP
/sbin/ip6tables -P OUTPUT ACCEPT

# Alle Pakete ordentlich zurückweisen
/sbin/ip6tables -A INPUT -p tcp -j REJECT --reject-with tcp-reset

