#!/bin/sh

# # ex - archive extractor
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
              *.tar.bz2)   tar xjf $1   ;;
	      *.tar.gz)    tar xzf $1   ;;
	      *.tar.xz)    tar xJf $1   ;;
	      *.bz2)       bunzip2 $1   ;;
	      *.rar)       unrar x $1     ;;
	      *.gz)        gunzip $1    ;;
	      *.tar)       tar xf $1    ;;
	      *.tbz2)      tar xjf $1   ;;
	      *.tgz)       tar xzf $1   ;;
	      *.zip)       unzip $1     ;;
	      *.Z)         uncompress $1;;
	      *.7z)        7z x $1      ;;
	      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# use bare git repo to  manage dotfiles
alias dotgit='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'


# Use neovim for vim if present.
[ -x "$(command -v nvim)" ] && alias vim="nvim" vimdiff="nvim -d"

export EDITOR=nvim

## package management
alias p='sudo pacman --color auto'
alias pacman='sudo pacman --color auto'
alias update='sudo pacman -Syyu'
alias fullupdate='yay -Syu'
alias pacman-clear='sudo pacman -Rsn $(pacman -Qdtq)'

#get fastest mirrors in your neighborhood
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"

#Cleanup orphaned packages
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'


# terminals
#alias alacritty="alacritty -e zsh"

# Quick access to rc and config files
alias bashrc='${=EDITOR} ~/.bashrc'
alias zshrc='${=EDITOR} ~/.zshrc'
alias sxhkdrc='${EDITOR} ~/.config/sxhkd/sxhkdrc'
alias strc='${EDITOR} ~/suckless/my/st/config.h'
alias vimrc='${EDITOR} ~/.config/nvim/init.vim'
alias aliasrc='${EDITOR} ~/.config/aliasrc'
alias alacrittyrc='${EDITOR} ~/.config/alacritty/alacritty.yml'
alias qtilerc='${EDITOR} ~/.config/qtile/config.py'

# kill
alias kk='killall kodi'

# rust replacements of GNU core utils
alias ls="exa -lah"
alias ls2="exa -lah -T -L=2"
alias ls3="exa -lah -T -L=3"
alias ls4="exa -lah -T -L=4"
alias ls5="exa -lah -T -L=5"
alias diff="diff --color=auto"

alias grep="rg"
#alias cat="bat"

# file handling
alias rm='rm -vI'
alias cp='cp -iv'
alias mv='mv -iv'
alias mkd='mkdir -pv'
alias vi='nvim'

# go back directories
alias d='dirs -v | head -10'
alias 0='cd ~0'
alias 1='cd ~1'
alias 2='cd ~2'
alias 3='cd ~3'
alias 4='cd ~4'
alias 5='cd ~5'
alias 6='cd ~6'
alias 7='cd ~7'
alias 8='cd ~8'
alias 9='cd ~9'

# avoid errors like 'zsh: no matches found:' when passing arguments to zsh without " "
alias ytdl='noglob ytdl'

alias dmenu='dmenu -h 30'

# Import the latest colors-set from wal.
. "${HOME}/.cache/wal/colors.sh"

# Create the alias.
alias dmenu_run='dmenu_run -nb "$color0" -nf "$color15" -sb "$color1" -sf "$color15"'

