# how to use a bare git repo for dotfiles

## Setup
```sh
git init --bare $HOME/.dotfiles
alias dotgit='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
dotgit remote add origin https://gitlab.com/ManuelHaake/dotfiles.git
```

## Replication
```sh
git clone --separate-git-dir=$HOME/.dotfiles https://gitlab.com/ManuelHaake/dotfiles.git my-dotfiles-tmp
rsync --recursive --verbose --exclude '.git' my-dotfiles-tmp/ $HOME/
rm --recursive my-dotfiles-tmp
```

## Configuration
```sh
dotgit config status.showUntrackedFiles no
dotgit remote set-url origin https://gitlab.com/ManuelHaake/dotfiles.git
```

## Usage
```sh
dotgit status
dotgit add .configfile
dotgit commit -m 'Add configfile'
dotgit push
```
